using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NDtw;
using NDtw.Examples;
using NDtw.Preprocessing;
using System.Collections.ObjectModel;
using System.Linq;
using System;
//using System.Diagnostics;

public class SampleDataComparer : MonoBehaviour
{
    public GameObject TextPrefab;
    public GameObject LineParent;

    public bool Recording = false;

    public void ToggleRecording()
    {
        Recording = !Recording;
    }

    Text _text;

    MultivariateDataSeriesRepository DataSeries;
    ObservableCollection<string> Entities;
    ObservableCollection<Variable> Variables;
    ObservableCollection<IPreprocessor> Preprocessors;
    ObservableCollection<DistanceMeasure> DistanceMeasures;
    private DistanceMeasure? _selectedDistanceMeasure;
    public DistanceMeasure? SelectedDistanceMeasure
    {
        get { return _selectedDistanceMeasure; }
        set
        {
            _selectedDistanceMeasure = value;
            //NotifyPropertyChanged(() => SelectedDistanceMeasure);
            //Recalculate();
        }
    }

    Dtw _dtw;

    ObservableCollection<Variable> SelectedVariables;
    ObservableCollection<string> SelectedEntities;

    float[] _memory;
    float[] _exampleData;

    private void DrawSeries(double[] seriesX, double[] seriesY)
    {
        _text.text = "Cost: " + _dtw.GetCost() + ", X length: " + _dtw.XLength + ", Y Length: " + _dtw.YLength + ", Normalized: " + (float)_dtw.GetCost() / Mathf.Sqrt(_dtw.XLength * _dtw.XLength + _dtw.YLength * _dtw.YLength);

        foreach (Transform tr in LineParent.GetComponentsInChildren<Transform>())
        {
            if (tr.gameObject != LineParent)
                Destroy(tr.gameObject);
        }

        GameObject line1 = Instantiate(new GameObject(), LineParent.transform);
        GameObject line2 = Instantiate(new GameObject(), LineParent.transform);
        LineRenderer lineRenderer1 = line1.gameObject.AddComponent<LineRenderer>();
        LineRenderer lineRenderer2 = line2.gameObject.AddComponent<LineRenderer>();
        //int w = Mathf.Max(seriesX.Length, seriesY.Length);

        //int buffer = 5;

        //int min = (int)Mathf.Min((float)seriesX.Min(), (float)seriesY.Min());
        //int max = (int)Mathf.Max((float)seriesX.Max(), (float)seriesY.Max());
        
        //int h = max - min;

        lineRenderer1.material = new Material(Shader.Find("Legacy Shaders/Particles/Alpha Blended Premultiply"));
        lineRenderer1.startColor = Color.red;
        lineRenderer1.endColor = Color.red;
        lineRenderer1.startWidth = 0.05f;
        lineRenderer1.endWidth = 0.05f;
        for (int x = 0; x < seriesX.Length; x++)
        {
            lineRenderer1.positionCount = seriesX.Length;
            lineRenderer1.SetPosition(x, new Vector3((x - seriesX.Length/2) / 20f, (float)seriesX[x] / 50, 0));
        }

        lineRenderer2.material = new Material(Shader.Find("Legacy Shaders/Particles/Alpha Blended Premultiply"));
        lineRenderer2.startColor = Color.green;
        lineRenderer2.endColor = Color.green;
        lineRenderer2.startWidth = 0.05f;
        lineRenderer2.endWidth = 0.05f;
        for (int x = 0; x < seriesY.Length; x++)
        {
            lineRenderer2.positionCount = seriesY.Length;
            lineRenderer2.SetPosition(x, new Vector3((x - seriesX.Length / 2) / 20f, (float)seriesY[x] / 50, 0));
        }

    }

    private IEnumerator Rerecord()
    {
        float sampleLength = 0.1f;
        //int frequency = 512;
        //AudioClip clip = Microphone.Start(null, true, 5, frequency);
        while (true)
        {
            yield return new WaitForSeconds(sampleLength);

            //int clipLength = (int)(sampleLength * frequency);

            //for (int i = 0; i < _memory.Length - clipLength; i++)
            //{
            //    _memory[i] = _memory[i + clipLength];
            //}
            //float[] newMemory = new float[clip.samples];
            //clip.GetData(newMemory, Microphone.GetPosition(null));
            //for (int i = 0; i < clipLength && i + clipLength < _memory.Length; i++)
            //{
            //    _memory[i + _memory.Length - clipLength] = newMemory[i];
            //}

            //string newMemoryMessage = "New memory: ";
            //foreach (float sample in newMemory)
            //{
            //    newMemoryMessage += sample.ToString() + ", ";
            //}
            //Debug.Log(newMemoryMessage);

            float[] newData = MicInput.GetData();
            for (int i = 0; i < _memory.Length - newData.Length; i++)
            {
                _memory[i] = _memory[i + newData.Length];
            }

            if (Recording)
            {
                for (int i = 0; i < _exampleData.Length - newData.Length; i++)
                {
                    _exampleData[i] = _exampleData[i + newData.Length];
                }
            }

            newData.CopyTo(_memory, _memory.Length - newData.Length);

            if (Recording)
                newData.CopyTo(_exampleData, _exampleData.Length - newData.Length);

            Recalculate();
        }
    }

    private void Start()
    {
        _memory = new float[44100];
        _exampleData = new float[44100];
        _text = GetComponent<Text>();

        InitializeData();
        SelectedVariables = new ObservableCollection<Variable>();
        SelectedVariables.Add(Variables[0]);
        //SelectedVariables.Add(Variables[1]);

        SelectedEntities = new ObservableCollection<string>();
        SelectedEntities.Add("Austria");
        SelectedEntities.Add("France");

        MicInput.StartMic();
        StartCoroutine(Rerecord());
    }
    private void Recalculate()
    {
        //if (!CanRecalculate)
        //    return;

        var seriesVariables = new List<SeriesVariable>();

        double[] memory = new double[_memory.Length / 100];
        double[] exampleData = new double[_exampleData.Length / 100];
        int count = 0;
        while (count < _memory.Length / 100)
        {
            double value = 0;
            for (int i = 0; i < 100; i++)
            {
                value += _memory[count * 100 + i];
            }
            memory[count++] = value;
        }

        count = 0;
        while (count < _exampleData.Length / 100)
        {
            double value = 0;
            for (int i = 0; i < 100; i++)
            {
                value += _exampleData[count * 100 + i];
            }
            exampleData[count++] = value;
        }

        foreach (var selectedVariable in SelectedVariables)
        {
            seriesVariables.Add(
                new SeriesVariable(
                    exampleData,
                    memory,
                    selectedVariable.Name,
                    selectedVariable.Preprocessor,
                    selectedVariable.Weight));
        }

        var seriesVariablesArray = seriesVariables.ToArray();

        var dtw = new Dtw(
            seriesVariablesArray,
            SelectedDistanceMeasure.Value,
            true,
            true,
            1,
            1,
            50);

        //if (MeasurePerformance)
        //{
        //    var swDtwPerformance = new Stopwatch();
        //    swDtwPerformance.Start();

        //    for (int i = 0; i < 250; i++)
        //    {
        //        var tempDtw = new Dtw(
        //            seriesVariablesArray,
        //            SelectedDistanceMeasure.Value,
        //            UseBoundaryConstraintStart,
        //            UseBoundaryConstraintEnd,
        //            UseSlopeConstraint ? SlopeConstraintDiagonal : (int?)null,
        //            UseSlopeConstraint ? SlopeConstraintAside : (int?)null,
        //            UseSakoeChibaMaxShift ? SakoeChibaMaxShift : (int?)null);
        //        var tempDtwPath = tempDtw.GetCost();
        //    }
        //    swDtwPerformance.Stop();
        //    OperationDuration = swDtwPerformance.Elapsed;
        //}

        _dtw = dtw;

        //Dtw = new Dtw(
        //    new[] { 4.0, 4.0, 4.5, 4.5, 5.0, 5.0, 5.0, 4.5, 4.5, 4.0, 4.0, 3.5 },
        //    new[] { 1.0, 1.5, 2.0, 2.5, 3.5, 4.0, 3.0, 2.5, 2.0, 2.0, 2.0, 1.5 },
        //    SelectedDistanceMeasure.Value,
        //    UseBoundaryConstraintStart,
        //    UseBoundaryConstraintEnd,
        //    UseSlopeConstraint ? SlopeConstraintDiagonal : (int?)null,
        //    UseSlopeConstraint ? SlopeConstraintAside : (int?)null,
        //    UseSakoeChibaMaxShift ? SakoeChibaMaxShift : (int?)null);
        DrawSeries(dtw.SeriesVariables[0].OriginalXSeries, dtw.SeriesVariables[0].OriginalYSeries);
    }

    private void InitializeData()
    {
        DataSeries = DataSeriesFactory.CreateMultivariateConsumptionByPurposeEurostat();

        Entities = new ObservableCollection<string>(DataSeries.GetEntities());

        var nonePreprocessor = new NonePreprocessor();
        Variables = new ObservableCollection<Variable>(DataSeries.GetVariables().Select(x => new Variable() { Name = x, Preprocessor = nonePreprocessor, Weight = 1 }));
        Preprocessors = new ObservableCollection<IPreprocessor>()
                                {
                                    nonePreprocessor,
                                    new CentralizationPreprocessor(),
                                    new NormalizationPreprocessor(),
                                    new StandardizationPreprocessor()
                                };

        DistanceMeasures = new ObservableCollection<DistanceMeasure>()
                                   {
                                       DistanceMeasure.Manhattan,
                                       DistanceMeasure.Euclidean,
                                       DistanceMeasure.SquaredEuclidean,
                                       DistanceMeasure.Maximum
                                   };

        SelectedDistanceMeasure = DistanceMeasure.Euclidean;
    }
}
