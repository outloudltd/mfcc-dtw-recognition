﻿using System;

namespace Comirva.Audio.Util.Maths
{
	class MathUtils
	{
		public static double Hypot(double x, double y)
		{
			return System.Math.Sqrt(x * x + y * y);
		}

		public static int FreqToIndex(double freq, double sampleRate, int spectrumLength)
		{
			/*N sampled points in time correspond to [0, N/2] frequency range */
			double fraction = freq / (sampleRate / 2);
			/*DFT N points defines [N/2 + 1] frequency points*/
			int i = (int)Math.Round(((spectrumLength / 2) + 1) * fraction);
			return i;
		}

		public static double RoundUp(double x)
		{
			return Math.Ceiling (x);
		}

		public static double CosineSimilarity(double[] a, double[] b)
		{
			double sum = 0;
			double sqrRootedA = 0;
			double sqrRootedB = 0;
			for (int i = 0; i < a.Length && i < b.Length; ++i) 
			{
				sum += (a[i] * b[i]);
				sqrRootedA += (a[i] * a[i]);
				sqrRootedB += (b[i] * b[i]);
			}
			sqrRootedA = Math.Sqrt (sqrRootedA);
			sqrRootedB = Math.Sqrt (sqrRootedB);
			return sum / (sqrRootedA * sqrRootedB);
		}
	}
}
