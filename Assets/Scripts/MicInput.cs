﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class MicInput {

	private static int sampleCount = 4800;
	static AudioClip audioClip;
	static int prevpos = 0;
	public static int SampleCount 
	{
		get 
		{ 
			return sampleCount; 
		}
		set 
		{
			if (value != sampleCount) 
			{
				sampleCount = value;
				audiodata = new float[sampleCount];
			}
		}
	}

	static float[] audiodata = new float[sampleCount];

	public static void StartMic()
	{
		audioClip = Microphone.Start(null, true, 5, AudioSettings.outputSampleRate);
		//Debug.Log ("audioClip length:" + audioClip.length);
		//Debug.Log("audioClip channels:" + audioClip.channels);
	}

	public static void StopMic()
	{
		Microphone.End (null);
	}

	public static float[] GetData()
	{
		int pos = Microphone.GetPosition( null );
		//print ("get data in pos:" + pos + " diff:" + (pos - prevpos));
		audioClip.GetData( audiodata,  prevpos);
		prevpos = pos;
		//print("name: " + name + " s length: " + s.Length);
		/*
		using (StreamWriter sw = File.CreateText (name + prevpos + ".csv"))
		{
			print ("writing to " + ((FileStream)(sw.BaseStream)).Name); 
			foreach (float f in audiodata)
				sw.WriteLine (f);

			sw.Close ();
		}
*/

		return audiodata;
	}
}
